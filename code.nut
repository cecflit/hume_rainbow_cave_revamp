//Hume's Rainbow Cave
//Squirrel Scripting
//v2.1.6

/*********************************
 *                               *
 *        Created by Hume        *
 * Updated and revamped by Narre *
 *                               *
 ********************************/

println("[DEBUG] levels/hume_rainbow_cave/code.nut loaded");
println("[INFO] Created by Hume");
println("[INFO] Updated and revamped by Narre");

this.code <- {a = "_", b = "_", c = "_", d = "_", e = "_", f = "_"};
function zero() {
	this.code.a <- "_";
	this.code.b <- "_";
	this.code.c <- "_";
	this.code.d <- "_";
	this.code.e <- "_";
	this.code.f <- "_";
	this.last <- "a";
}

zero();

instruction <- TextObject();
instruction.set_anchor_point(ANCHOR_TOP_LEFT);
instruction.set_pos(30, 60);
instruction.set_text_color(1, 0.5, 0, 1);
instruction.set_text("Enter the bonus levels code:");
instruction.set_visible(true);

Text.set_anchor_point(ANCHOR_TOP_LEFT);
Text.set_pos(30, 120);
Text.fade_in(0.2);

function add(number) {
	Text.set_text_color(1, 1, 1, 1);
	if(this.last != "g") {
		this.code[this.last] <- number;
		this.cd <- this.code.a + this.code.b + this.code.c + this.code.d + this.code.e + this.code.f;
		Text.set_text(this.cd);
		if(this.last == "f") {
			this.last <- "g";
			if(this.cd == "567418") {
				Text.set_wrap_width(800);
				Text.set_text(_("567418 - Bonus levels are unlocked!"));
				Text.set_font("big");
				Text.set_text_color(0, 1, 0, 1);
				pipe.goto_node(1);
				sector.Tux.trigger_sequence("fireworks");
			} else if(this.cd == "489641") {
				Text.set_text(_("489641 - 1 bonus coin!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_coins(1);
				zero();
			} else if(this.cd == "028976") {
				Text.set_text(_("028976 - 10 bonus coins!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_coins(10);
				zero();
			} else if(this.cd == "360126") {
				Text.set_text(_("360126 - 50 bonus coins!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_coins(50);
				zero();
			} else if(this.cd == "289221") {
				Text.set_text(_("289221 - 100 bonus coins!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_coins(100);
				zero();
			} else if(this.cd == "265813") {
				Text.set_text(_("265813 - 500 bonus coins!"));
				Text.set_text_color(1, 1, 0, 1);
				for(i <- 0; i < 5; ++i) {
					sector.Tux.add_coins(100);
					wait(0.2);
				}
				zero();
			} else if(this.cd == "310412") {
				Text.set_text(_("310412 - A bonus egg!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_bonus("grow");
				zero();
			} else if(this.cd == "598126") {
				Text.set_text(_("598126 - A bonus fireflower!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_bonus("fireflower");
				zero();
			} else if(this.cd == "860312") {
				Text.set_text(_("860312 - A bonus iceflower!"));
				Text.set_text_color(1, 1, 0, 1);
				sector.Tux.add_bonus("iceflower");
				zero();
			} else {
				Text.set_text("Wrong code!");
				Text.set_text_color(1, 0, 0, 1);
				zero();
			}
		} else if(this.last == "e") this.last <- "f";
		else if(this.last == "d") this.last <- "e";
		else if(this.last == "c") this.last <- "d";
		else if(this.last == "b") this.last <- "c";
		else if(this.last == "a") this.last <- "b";
	}
}
