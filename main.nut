//Hume's Rainbow Cave
//Squirrel Scripting
//v2.1.6

/*
	HH  HH  UU  UU  MM  MM  EEEEEE  ''   SSSSS
	HH  HH  UU  UU  MMMMMM  EE      ''  SS
	HHHHHH  UU  UU  MM  MM  EEEE    ''   SSSS
	HH  HH  UU  UU  MM  MM  EE              SS
	HH  HH   UUUU   MM  MM  EEEEEE      SSSSS

	RRRRR    AAAA   IIIIII  NN  NN  BBBBB    OOOO   WW  WW
	RR  RR  AA  AA    II    NNN NN  BB  BB  OO  OO  WW  WW
	RRRRR   AAAAAA    II    NNNNNN  BBBBB   OO  OO  WW  WW
	RR RR   AA  AA    II    NN NNN  BB  BB  OO  OO  WWWWWW
	RR  RR  AA  AA  IIIIII  NN  NN  BBBBB    OOOO   WW  WW
  
	 CCCC    AAAA   VV  VV  EEEEEE
	CC  CC  AA  AA  VV  VV  EE
	CC      AAAAAA  VV  VV  EEEE
	CC  CC  AA  AA   VVVV   EE
	 CCCC   AA  AA    VV    EEEEEE
  
-SCRIPTS
  
	#################################
	#                               #
	#        Created by Hume        #
	# Updated and revamped by Narre #
	#                               #
	#################################

Contents:

+----------------------------------------------------------------------+
| main.nut                                                             |
| -1 Documentation                                            line 59  |
|  -1.1 Functions                                             line 63  |
|   -1.1.1 import("levels/hume_rainbow_cave/main.nut")        line 65  |
|   -1.1.2 secret_area("secret area")                         line 67  |
|   -1.1.3 complete_level(level_group, level, sequence)       line 69  |
|   -1.1.4 found_crystal(crystal)                             line 73  |
|   -1.1.5 finish_level_group(level_group, level)             line 75  |
|   -1.1.6 warp_to(sector, spawnpoint)                        line 77  |
|   -1.1.7 save_achievements()                                line 79  |
|   -1.1.8 achievement_init()                                 line 81  |
|  -1.2 External scripts                                      line 84  |
|  -1.3 code.nut                                              line 89  |
|   -1.3.1 import("levels/hume_rainbow_cave/code.nut")        line 92  |
|   -1.3.2 zero()                                             line 94  |
|   -add(number)                                              line 96  |
|  -1.4 octachoron.nut                                        line 99  |
|   -1.4.1 import("levels/hume_rainbow_cave/octachoron.nut")  line 102 |
|   -1.4.2 add(number)                                        line 104 |
|   -1.4.3 messup()                                           line 106 |
| -2 Scripts                                                  line 109 |
+----------------------------------------------------------------------+

== 1 Documentation ==
 All achievements in this worldmap are scriped here.
 Play the first level for more info.

 =- 1.1 Functions -=
    Script usage...
  -- 1.1.1 import("levels/hume_rainbow_cave/main.nut") --
     ...to load these functions.
  -- 1.1.2 secret_area("secret area") --
     ...to check for a secret area. Secret areas are called "sa1", "sa2", "sa3", ..., "sa42".
  -- 1.1.3 complete_level(level_group, level, sequence) --
     ...to complete a level. Level groups are called "red", "orange", "yellow", "green", "blue", "purple" and "white".
	Red levels are called "r1" to "r6", other colours similarly.
	Sequences are "endsequence", "stoptux" and "fireworks".
  -- 1.1.4 found_crystal(crystal) =
     ...to check for a crystal. Crystals are called just as level groups.
  -- 1.1.5 finish_level_group(level_group, level) --
     ...to finish a level group.
  -- 1.1.6 warp_to(sector, spawnpoint) --
     ...to warp to the target sector and spawnpoint
  -- 1.1.7 save_achievements() --
     ...to save achievements
  -- 1.1.8 achievement_init() --
     ...to activate achievements in "Unlocked Achievements".
 
 =- 1.2 External scripts -=
    Some levels use scripts from other files:
     "code.nut" in "Bonus Levels Gate"
     "octachoron.nut" in "Octachoral Test"
  
 =- 1.3 code.nut -=
    Functions:
    Script usage...
  -- 1.3.1 import("levels/hume_rainbow_cave/code.nut") --
     ...to load these functions.
  -- 1.3.2 zero() --
     ...to reset the code to "_ _ _ _ _ _".
  -- 1.3.3 add(number) --
     ...to add a number to the code.
 
 =- 1.4 octachoron.nut -=
    Functions:
    Script usage...
  -- 1.4.1 import("levels/hume_rainbow_cave/octachoron.nut") --
     ...to load these functions.
  -- 1.4.2 add(number) --
     ...to add a number to the result.
  -- 1.4.3 messup() --
     ...to fail the question.

== 2 scripts ==

Do not read the following text. You won't understand it.
*******************************************************************************/
 
println("[DEBUG] levels/hume_rainbow_cave/main.nut loaded");
println("[INFO] WELCOME TO HUME'S RAINBOW CAVE!");
println("[INFO] Created by Hume");
println("[INFO] Updated and revamped by Narre");

Text.set_anchor_point(ANCHOR_TOP_LEFT);
Text.set_pos(30, 120);
achievement_ind <- ["goal", "secret", "crystal", "red", "orange", "yellow", "green"," blue", "purple", "white"];
levels <- { red = ["r1", "r2", "r3", "r4", "r5", "r6"], orange = ["o1", "o2", "o3", "o4", "o5", "o6"], yellow = ["y1", "y2", "y3", "y4", "y5", "y6"], green = ["g1", "g2", "g3", "g4", "g5", "g6"], blue = ["b1", "b2", "b3", "b4", "b5", "b6"], purple = ["p1", "p2", "p3", "p4", "p5", "p6"], white = ["w1", "w2", "w3", "w4", "w5", "w6"]};
colours <- ["red", "orange", "yellow", "green", "blue", "purple", "white"];
secret_areas <- ["sa1", "sa2", "sa3", "sa4", "sa5", "sa6", "sa7", "sa8", "sa9", "sa10", "sa11", "sa12", "sa13", "sa14", "sa15", "sa16", "sa17", "sa18", "sa19", "sa20", "sa21", "sa22", "sa23", "sa24", "sa25", "sa26", "sa27", "sa28", "sa29", "sa30", "sa31", "sa32", "sa33", "sa34", "sa35", "sa36", "sa37", "sa38", "sa39", "sa40", "sa41", "sa42"];

/**********************************
 *                                *
 *    Allocate space in memory    *
 *                                *
 **********************************/

if(!("hume_rainbow_cave" in state)) {
	state.hume_rainbow_cave <- {};
	state.hume_rainbow_cave.levels <- {};
	foreach(colour in colours) {
		state.hume_rainbow_cave.levels[colour] <- {};
		foreach(level in levels[colour]) state.hume_rainbow_cave.levels[colour][level] <- false;
	}
	state.hume_rainbow_cave.secrets <- {};
	foreach(name in secret_areas) state.hume_rainbow_cave.secrets[name] <- false;
	state.hume_rainbow_cave.crystals <- {};
	foreach (name in colours) state.hume_rainbow_cave.crystals[name] <- false;
	state.hume_rainbow_cave.achievements <- {};
	foreach (name in achievement_ind) state.hume_rainbow_cave.achievements[name] <- false;

/****************************
 *  hume_rainbow_cave       *
 *   -levels                *
 *    -red                  *
 *     -r1..r6              *
 *    -orange               *
 *     -o1..o6              *
 *    -yellow               *
 *     -y1..y6              *
 *    -green                *
 *     -g1..g6              *
 *    -blue                 *
 *     -b1..b6              *
 *    -purple               *
 *     -p1..p6              *
 *    -white                *
 *     -w1..w6              *
 *   -secrets               *
 *    -sa1..sa42            *
 *   -crystals              *
 *    -red, orange, yellow, *
 *     green, blue, purple, *
 *     white                *
 *   -achievements          *
 *    -goal, secret, crystal*
 *    -red, orange, yellow, *
 *     green, blue, purple, *
 *     white                *
 ****************************/
}

achievements <- state.hume_rainbow_cave;

/**************************************************
 *                                                *
 *      "Unlocked achievements" init.script       *
 *                                                *
 **************************************************/

function achievement_init() {
	achievement_goal <- 0;
	achievement_secret <- 0;
	achievement_crystal <- 0;
	achievement_red <- 0;
	achievement_orange <- 0;
	achievement_yellow <- 0;
	achievement_green <- 0;
	achievement_blue <- 0;
	achievement_purple <- 0;
	achievement_white <- 0;

	foreach(name in secret_areas) if(achievements.secrets[name]) ++achievement_secret;
	foreach(name in colours) if(achievements.crystals[name]) ++achievement_crystal;
	foreach(name in levels.red) if(achievements.levels.red[name]) ++achievement_red;
	foreach(name in levels.orange) if(achievements.levels.orange[name]) ++achievement_orange;
	foreach(name in levels.yellow) if(achievements.levels.yellow[name]) ++achievement_yellow;
	foreach(name in levels.green) if(achievements.levels.green[name]) ++achievement_green;
	foreach(name in levels.blue) if(achievements.levels.blue[name]) ++achievement_blue;
	foreach(name in levels.purple) if(achievements.levels.purple[name]) ++achievement_purple;
	foreach(name in levels.white) if(achievements.levels.white[name]) ++achievement_white;

	achievement_goal <- achievement_red + achievement_orange + achievement_yellow + achievement_green + achievement_blue + achievement_purple + achievement_white;
	if(achievement_goal == 42 && achievement_secret == 42 && achievement_crystal == 7) pipe.fade(0, 1);
}

function save_achievements() {
	state.hume_rainbow_cave <- achievements;
}

/******************************************
 *                                        *
 *          Unlocking functions           *
 *                                        *
 *----------------------------------------*
 *                                        *
 *  complete_level(group, level, sequence)*
 *  secret_area(sa)                       *
 *  finish_level_group(colour, level)     *
 *  found_crystal(crystal)                *
 *                                        *
 ******************************************/
 
function complete_level(group, level, sequence) {
	achievements.levels[group][level] <- true;
	save_achievements();
	sector.Tux.trigger_sequence(sequence);
}

function secret_area(sa) {
	achievements.secrets[sa] <- true;
	save_achievements();
	get_achievement <- true;
	foreach(name in secret_areas) get_achievement <- get_achievement && achievements.secrets[name];

	if(get_achievement && !(achievements.achievements.secret)) {
		achievements.achievements.secret <- true;
		save_achievements();
		Text.set_text(_("You've found all secret areas!"));
		play_sound("sounds/invincible_start.ogg");
		Text.fade_in(0.2);
		wait(3);
		Text.fade_out(0.2);
	}
}

function finish_level_group(colour, level) {
	complete_level(colour, level, "fireworks");
	get_achievement <- false;
	if(!(achievements.achievements[colour])) {
		invincible();
		achievements.achievements[colour] <- true;
		save_achievements();
		switch(colour) {
			case "blue": Text.set_text(_("You've completed all blue levels!")); break;
			case "green": Text.set_text(_("You've completed all green levels!")); break;
			case "orange": Text.set_text(_("You've completed all orange levels!")); break;
			case "purple": Text.set_text(_("You've completed all purple levels!")); break;
			case "red": Text.set_text(_("You've completed all red levels!")); break;
			case "white": Text.set_text(_("You've completed all white levels!")); break;
			case "yellow": Text.set_text(_("You've completed all yellow levels!")); break;
		}
		play_sound("sounds/invincible_start.ogg");
		Text.fade_in(0.2);
		wait(3);
		Text.fade_out(0.2);
		get_achievement <- true;
	}

	foreach(name in colours) {
		get_achievement <- get_achievement && achievements.achievements[name];
		println(name + " is " + achievements.achievements[name]);
	}

	if(get_achievement && !(achievements.achievements.goal)) {
		achievements.achievements.goal <- true;
		save_achievements();
		Text.set_text(_("You've completed every single level!"));
		play_sound(_("sounds/invincible_start.ogg"));
		Text.fade_in(0.2);
		wait(3);
		Text.fade_out(0.2);
	}
}

function found_crystal(crystal) {
	if(!(achievements.crystals[crystal])) {
		play_sound("sounds/invincible_start.ogg");

		achievements.crystals[crystal] <- true;
		save_achievements();
		get_achievement <- true;

		foreach(name in colours) get_achievement <- get_achievement && achievements.crystals[name];
		if(get_achievement && !(achievements.achievements.crystal)) {
			achievements.achievements.crystal <- true;
			save_achievements();
			Text.set_text(_("You've collected all bonus crystals!"));
			play_sound("sounds/invincible_start.ogg");
			Text.fade_in(0.2);
			wait(3);
			Text.fade_out(0.2);
		}
	}
}

function warp_to(sector, spawnpoint) {
	play_sound("sounds/warp.wav");
	Tux.deactivate();
	Camera.set_mode("manual");
	Tux.set_visible(false);
	Effect.fade_out(2);
	wait(2);
	Level.spawn(sector, spawnpoint);
	Effect.fade_in(0);
	Tux.activate();
	Camera.set_mode("normal");
	Tux.set_visible(true);
}
