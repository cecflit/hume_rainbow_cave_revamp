//Hume's Rainbow Cave
//Squirrel Scripting
//v2.1.6

/*********************************
 *                               *
 *        Created by Hume        *
 * Updated and revamped by Narre *
 *                               *
 ********************************/

println("[DEBUG] levels/hume_rainbow_cave/octachoron.nut loaded");
println("[INFO] Created by Hume");
println("[INFO] Updated and revamped by Narre");

function initialise() {
	cd <- 0;
	attempts <- 0;
	randh <- rand();

	a <- randh & 255;
	b <- (randh >> 8) & 255;
	randh <- (randh >> 16) & 1;

	if(randh) {
		operator <- " + ";
		c <- a + b;
	} else {
		operator <- " - ";
		c <- a - b;
		if(c < 0) {
			randh <- a;
			a <- b;
			b <- randh;
			c <- -c;
		}
	}

	Text.set_anchor_point(ANCHOR_TOP_LEFT);
	Text.set_pos(30, 110);
	Text.set_text_color(1, 0.5, 0, 1);
	Text.set_back_fill_color(0.5, 0.5, 0.5, 0.5);
	Text.set_front_fill_color(0, 0, 0, 0);
	Text.set_text(a + operator + b + " = ?");
	Text.set_visible(true);

	TextArray.add_text("");
	TextArray.set_font("big");
	TextArray.set_anchor_point(ANCHOR_TOP_LEFT);
	TextArray.set_pos(30, 50);
	TextArray.set_roundness(10);
	TextArray.set_text_color(1, 1, 0, 1);
	TextArray.set_back_fill_color(0.5, 0.5, 0.5, 0.5);
	TextArray.set_front_fill_color(0, 0, 0, 0);
	TextArray.set_visible(true);
}

function messup() {
	Text.set_text(_("WRONG!"));
	Text.set_text_color(1, 0, 0, 1);
	randh <- a & 7;
	if(!randh) door1.goto_node(1);
	else if(randh == 1) door2.goto_node(1);
	else if(randh == 2) door3.goto_node(1);
	else if(randh == 3) door4.goto_node(1);
	else if(randh == 4) door5.goto_node(1);
	else if(randh == 5) door6.goto_node(1);
	else if(randh == 6) door7.goto_node(1);
	else if(randh == 7) door8.goto_node(1);
}

function add(number) {
	if(attempts < 3) {
		cd <- cd * 10 + number;
		++attempts;
		Text.set_text(a + operator + b + " = " + cd);
		if(cd == c) {
			Text.set_text(_("CORRECT!"));
			Text.set_text_color(0, 1, 0, 1);
			attempts <- 3;
			door1.goto_node(1);
			door2.goto_node(1);
			door3.goto_node(1);
			door4.goto_node(1);
			door5.goto_node(1);
			door6.goto_node(1);
			door7.goto_node(1);
			door8.goto_node(1);
		} else if(attempts == 3) messup();
	}
}
